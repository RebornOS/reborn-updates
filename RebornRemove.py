# Reborn Maintenance App Trigger Handlers
# Created by Azaiel for RebornOS
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# RebornOS Discord: Azaiel

# This ensures that the Gtk version is 3.0
import subprocess
import gi
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("Unnecessary Packages")
try:
    import httplib
except:
    import http.client as httplib

workingDirectory = os.path.dirname(os.path.realpath(__file__))

# Create Handlers (Triggers) for each item
class Handler:

# Close the window
    def onDestroy5(self, *args):
        Gtk.main_quit()

################################################################################
############################### Buttons ########################################
################################################################################

# Save Program List
    def onEntryRemove(self, pkgtxt):
        global enteredText2
        enteredText2 = pkgtxt.get_text()
        print("Entered Text: ", enteredText2)
        print()

# Recover From
    def onRemove(self, button):
        print("Entered Text: ", enteredText2)
        Notify.Notification.new("Removing...").show()
        os.system('xterm -e sudo pacman -Rdd '  + enteredText2 + ' --noconfirm')
        Gtk.main_quit()

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(workingDirectory + "/RebornMaintenance.glade")
builder.connect_signals(Handler())

window1 = builder.get_object("Reborn5")
window1.show_all()

Gtk.main()

Notify.uninit()
