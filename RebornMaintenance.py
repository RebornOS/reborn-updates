# Reborn Maintenance App Trigger Handlers
# Created by Azaiel for RebornOS
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# RebornOS Discord: Azaiel

# This ensures that the Gtk version is 3.0
import subprocess
import gi
import os
import time
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("Reborn Updates and Maintenance")
try:
    import httplib
except:
    import http.client as httplib

workingDirectory = os.path.dirname(os.path.realpath(__file__))
# Create Handlers (Triggers) for each item

# Writing bash variable that call in specific functions from our reborn-updates script
keys = 'bash ' + workingDirectory + '/reborn-updates Keys'
installMycroft = 'bash ' + workingDirectory + '/reborn-updates Mycroft'
installAnbox = 'bash ' + workingDirectory + '/reborn-updates Anbox'
installApricity = 'bash ' + workingDirectory + '/reborn-updates Apricity'
installBudgie = 'bash ' + workingDirectory + '/reborn-updates Budgie'
installCinnamon = 'bash ' + workingDirectory + '/reborn-updates Cinnamon'
installDeepin = 'bash ' + workingDirectory + '/reborn-updates Deepin'
installEnlightenment = 'bash ' + workingDirectory + '/reborn-updates Enlightenment'
installGnome = 'bash ' + workingDirectory + '/reborn-updates Gnome'
installi3 = 'bash ' + workingDirectory + '/reborn-updates i3'
installPlasma = 'bash ' + workingDirectory + '/reborn-updates Plasma'
installLXQt = 'bash ' + workingDirectory + '/reborn-updates LXQt'
installMate = 'bash ' + workingDirectory + '/reborn-updates Mate'
installOpenbox = 'bash ' + workingDirectory + '/reborn-updates Openbox'
installPantheon = 'bash ' + workingDirectory + '/reborn-updates Pantheon'
installWindows = 'bash ' + workingDirectory + '/reborn-updates Windows'
installAddons = 'bash ' + workingDirectory + '/reborn-updates Addons'

# Check for Internet connection
conn = httplib.HTTPConnection("www.google.com", timeout=5)
try:
    conn.request("HEAD", "/")
    conn.close()
except:
    conn.close()
    Notify.Notification.new("Lacking Internet connection. Some options may not work").show()

class Handler:
    def __init__(self):
        self.listoptions1 = [0,0,0,0,0]
        self.listoptions2 = [0,0,0,0,0,0]

# Close the window
    def onDestroy(self, *args):
        Gtk.main_quit()

################################################################################
############################### First Tab ######################################
################################################################################

# Install Mycroft
    def clickMycroft(self, button):
        subprocess.Popen(['xterm', '-e', installMycroft])

# Install Anbox
    def clickAnbox(self, button):
        subprocess.Popen(['xterm', "-e", installAnbox])

# Install Security Updates
    def installSecurity(self, button):
        subprocess.Popen(["xterm", "-e", keys])

################################################################################
############################### Second Tab ######################################
################################################################################

# Install Apricity
    def clickApricity(self, button):
        subprocess.Popen(['xterm', '-e', installApricity])

    # Install Budgie
    def clickBudgie(self, button):
        subprocess.Popen(['xterm', '-e', installBudgie])

    # Install Cinnamon
    def clickCinnamon(self, button):
        subprocess.Popen(['xterm', '-e', installCinnamon])

    # Install Deepin
    def clickDeepin(self, button):
        subprocess.Popen(['xterm', '-e', installDeepin])

    # Install Enlightenment
    def clickEnlightenment(self, button):
        subprocess.Popen(['xterm', '-e', installEnlightenment])

    # Install GNOME
    def clickGnome(self, button):
        subprocess.Popen(['xterm', '-e', installGnome])

    # Install i3
    def clicki3(self, button):
        subprocess.Popen(['xterm', '-e', installi3])

    # Install Plasma
    def clickPlasma(self, button):
        subprocess.Popen(['xterm', '-e', installPlasma])

    # Install LXQt
    def clickLXQt(self, button):
        subprocess.Popen(['xterm', '-e', installLXQt])

    # Install Mate
    def clickMate(self, button):
        subprocess.Popen(['xterm', '-e', installMate])

    # Install Openbox
    def clickOpenbox(self, button):
        subprocess.Popen(['xterm', '-e', installOpenbox])

    # Install Pantheon
    def clickPantheon(self, button):
        subprocess.Popen(['xterm', '-e', installPantheon])

    # Install Windows
    def clickWindows(self, button):
        subprocess.Popen(['xterm', '-e', installWindows])

    # Install XFCE
    def clickXFCE(self, button):
        subprocess.Popen(['xterm', '-e', installXFCE])

################################################################################
############################### Third Tab ######################################
################################################################################

    def onClearCache(self, switch, state):
        if state == True:
            self.listoptions1[0]=1
        else:
            self.listoptions1[0]=0

    def onCleanJournal(self, switch, state):
        if state == True:
            self.listoptions1[1]=1
        else:
            self.listoptions1[1]=0

    def onCleanPackages(self, switch, state):
        if state == True:
            self.listoptions1[2]=1
        else:
            self.listoptions1[2]=0

    def onRankMirrors(self, switch, state):
        if state == True:
            self.listoptions1[3]=1
        else:
            self.listoptions1[3]=0

    def onUnnecessaryPrograms(self, switch, state):
        if state == True:
            self.listoptions1[4]=1
        else:
            self.listoptions1[4]=0

    def clickApply1(self, button):
        print(self.listoptions1)
        if self.listoptions1[0] == 1:
            print("Clearing Cache...")
            os.system('bash ' + workingDirectory + '/reborn-updates ClearCache')
            Notify.Notification.new("Cache Cleared").show()

        if self.listoptions1[1] == 1:
            print("Cleaning Journal...")
            os.system('bash ' + workingDirectory + '/reborn-updates ClearJournal')
            Notify.Notification.new("Journal Cleaned").show()

        if self.listoptions1[2] == 1:
            print("Cleaning Package Cache...")
            os.system('bash ' + workingDirectory + '/reborn-updates Clean')
            Notify.Notification.new("Package Cache Cleaned").show()

        if self.listoptions1[3] == 1:
            print("Ranking Mirrors...")
            Notify.Notification.new("Ranking Mirrors...").show()
            os.system('bash ' + workingDirectory + '/reborn-updates RankMirrors')
            Notify.Notification.new("Mirrors Ranked").show()

        if self.listoptions1[4] == 1:
            Notify.Notification.new("Generating Package List...").show()
            os.system('rm -f /tmp/package-files.txt')
            os.system('pacman -Qqtd > /tmp/package-files.txt')
            os.system('python3 ' + workingDirectory + '/RebornUnnecessary.py')

################################################################################
############################### Fourth Tab #####################################
################################################################################

    def onSaveRecoverPackages(self, switch, state):
        if state == True:
            self.listoptions2[0]=1
        else:
            self.listoptions2[0]=0

    def onRebuildGrub(self, switch, state):
        if state == True:
            self.listoptions2[1]=1
        else:
            self.listoptions2[1]=0

    def onReinstallGrubEFI(self, switch, state):
        if state == True:
            self.listoptions2[2]=1
        else:
            self.listoptions2[2]=0

    def onReinstallGrubBIOS(self, switch, state):
        if state == True:
            self.listoptions2[3]=1
        else:
            self.listoptions2[3]=0

    def onDowngrade(self, switch, state):
        if state == True:
            self.listoptions2[4]=1
        else:
            self.listoptions2[4]=0

    def onRemovePackage(self, switch, state):
        if state == True:
            self.listoptions2[5]=1
        else:
            self.listoptions2[5]=0

    def clickApply2(self, button):
        print(self.listoptions2)
        if self.listoptions2[0] == 1:
            print("Save/Recover Packages...")
            os.system('python3 ' + workingDirectory + '/RebornSelect.py')
            Notify.Notification.new("Operation Complete").show()

        if self.listoptions2[1] == 1:
            print("Reinstalling Grub...")
            Notify.Notification.new("Reinstalling Grub...").show()
            os.system('xterm -e sudo pacman -S grub --force --noconfirm')
            Notify.Notification.new("Grub has Been Reinstalled").show()

        if self.listoptions2[2] == 1:
            print("Rebuilding Grub (BIOS)...")
            os.system('bash ' + workingDirectory + '/reborn-updates Grub2')
            Notify.Notification.new("Grub is Rebuilt").show()

        if self.listoptions2[3] == 1:
            print("Rebuilding Grub (EFI)...")
            os.system('python3 ' + workingDirectory + '/reborn-updates Grub')
            Notify.Notification.new("Grub is Rebuilt").show()

        if self.listoptions2[4] == 1:
            print("Downgrading interface started...")
            os.system('python3 ' + workingDirectory + '/RebornDowngrade.py')
            Notify.Notification.new("Package Downgraded").show()

        if self.listoptions2[5] == 1:
            print("Package Removal interface started...")
            os.system('python3 ' + workingDirectory + '/RebornRemove.py')
            Notify.Notification.new("Package Removed").show()

################################################################################
############################### Fifth Tab ######################################
################################################################################

    # Install Addons
    def clickInstallAddons(self, button):
        subprocess.Popen(['xterm', '-e', installAddons])

    # Control Mycroft
    def clickControlMycroft(self, button):
        os.system('pkexec mycroft.sh')

    # Control Anbox
    def clickControlAnbox(self, button):
        os.system('pkexec anbox-reborn.sh')

    # Share Files
    def clickShareFiles(self, button):
        os.system('filedrawer')

    # VPN Manager
    def clickVPNManager(self, button):
        os.system('vpn-manager.sh')

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(workingDirectory + "/RebornMaintenance.glade")
builder.connect_signals(Handler())

window2 = builder.get_object("Reborn")
window2.show_all()

Gtk.main()

Notify.uninit()
